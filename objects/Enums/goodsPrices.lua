---@enum goodsPrice
local goodsPrice = {}

goodsPrice["meat"] = { gold = 8, quantity = 1 }
goodsPrice["cheese"] = { gold = 8, quantity = 1 }
goodsPrice["apples"] = { gold = 8, quantity = 1 }
goodsPrice["bread"] = { gold = 8, quantity = 1 }
goodsPrice["wheat"] = { gold = 32, quantity = 1 }
goodsPrice["flour"] = { gold = 32, quantity = 1 }
goodsPrice["wood"] = { gold = 4, quantity = 1 }
goodsPrice["stone"] = { gold = 14, quantity = 1 }
goodsPrice["iron"] = { gold = 45, quantity = 1 }
goodsPrice["tar"] = { gold = 30, quantity = 1 }
goodsPrice["spear"] = { gold = 20, quantity = 1 }
goodsPrice["bow"] = { gold = 31, quantity = 1 }
goodsPrice["mace"] = { gold = 58, quantity = 1 }
goodsPrice["crossbow"] = { gold = 58, quantity = 1 }
goodsPrice["sword"] = { gold = 58, quantity = 1 }
goodsPrice["pike"] = { gold = 36, quantity = 1 }
goodsPrice["leatherArmor"] = { gold = 25, quantity = 1 }
goodsPrice["shield"] = { gold = 58, quantity = 1 }
goodsPrice["ale"] = { gold = 20, quantity = 1 }
goodsPrice["hop"] = { gold = 15, quantity = 1 }

return goodsPrice
